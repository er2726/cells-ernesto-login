{
  const {
    html,
  } = Polymer;
  /**
    `<cells-ernesto-login>` Description.

    Example:

    ```html
    <cells-ernesto-login></cells-ernesto-login>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-ernesto-login | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsErnestoLogin extends Polymer.mixinBehaviors([ CellsBehaviors.i18nBehavior, ], Polymer.Element) {
    static get is() {
      return 'cells-ernesto-login';
    }

    static get properties() {
      return {
        nombre: {
          type: String,
          value: ""
        },
        pass: {
          type: String,
          value: ""
        },
        loged : {
          type: Boolean,
          notify: true
        }
      };
    }
    validar()  {
      if (this.nombre == "er" && this.pass == "emer") {
        this.set("loged", true);
        console.log(this.loged)
        this.dispatchEvent(new CustomEvent('login-succes',{detail:this.nombre}))
      }else {
        console.log(this.loged)
       this.dispatchEvent(new CustomEvent('login-error',{detail:this.nombre}))
      }
    }
    static get template() {
      return html `
      <style include="cells-ernesto-login-styles cells-ernesto-login-shared-styles"></style>
      <slot></slot>
        <div class="input-group">
          <h1>Inicia Sesión</h1>
        </div>
        <div class="form">
          <div class="input-group">
            <h4>Nombre:</h4>
            <input id="nombre" label="Usuario" value="{{nombre::input}}">
          </div>
          <br/>
          <div class="input-group">
            <h4>Contraseña:</h4>
            <input id="pass" type="password" label="Contraseña" value="{{pass::input}}">
          </div><br/><br/>
          <div class="input-group">
             <button  on-click="validar" value="Enviar">Enviar</button> <br/>
          </div>
        </div>
      `;
    }
  }

  customElements.define(CellsErnestoLogin.is, CellsErnestoLogin);
}
